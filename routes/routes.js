/**
*  ROUTES.JS
*  A set of routes for unauthenticated users.
**/
module.exports = function (app, passport) {


    app.get('/', function (req, res) {
        res.render('index', {
            title: "Home"
        });
    });

    app.get('/login', function (req, res) {
        res.render('login', {
            title: "Login",
            message: req.flash('loginMessage'),
            redirect: req.query.redirect
        });
    });

    app.get('/register', function (req, res) {
        res.render('register', {
            title: "Register",
            message: req.flash('signupMessage')
        });
    });

    app.get('/about', function (req, res) {
        res.render('about', {
            title: "About"
        });
    });

    app.get('/ui-test', function (req, res) {
        res.render('uitest', {
            title: "UI Test"
        });
    });
};
