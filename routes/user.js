/**
 *  USER.JS
 *  Routes used by a logged in user. Will check that the user is in fact logged
 *  in first, using the isLoggedIn middleware function.
 **/
var Models = require('../models/models.js');
var path = require('path');
var ourMiddleware = require('./middleware.js');
var complexQueries = require('./complexQueries.js');
var multer = require('multer');
var md5 = require('js-md5');
var fs = require('fs');

module.exports = function (app, passport) {

    app.get('/', function (req, res, next) {
        if (req.isAuthenticated()) {
            res.redirect('/home');
        } else {
            next();
        }
    });

    app.get('/home', ourMiddleware.isLoggedIn, function (req, res) {
        ourMiddleware.getUserApplications(req.user.username, true).then(function (applications) {
            res.render('home', {
                title: 'Home',
                user: req.user,
                applications: applications,
                successMessage: req.flash('successMessage')
            });
        });
    });

    app.get('/welcome', ourMiddleware.isLoggedIn, function (req, res) {
        res.render('welcome', {
            title: 'Welcome',
            user: req.user,
            sessionUser: JSON.stringify(req.session.passport.user, null, '\t')
        });
    });

    app.post('/welcome', ourMiddleware.isLoggedIn, function (req, res) {
        Models.User.findOne({
                where: {
                    username: req.user.username
                }
            })
            .done(function (userInstance) {
                var updateVals = {};
                req.user.addressFirstLine = updateVals.addressFirstLine = req.body.addressFirstLine;
                req.user.addressBuildingNo = updateVals.addressBuildingNo = req.body.addressBuildNo ? req.body.addressBuildNo : "";
                req.user.addressSecondLine = updateVals.addressSecondLine = req.body.addressSecondLine;
                req.user.addressPostalCode = updateVals.addressPostalCode = req.body.addressPostcode;
                req.user.addressCountryCode = updateVals.addressCountryCode = "en-GB";
                req.user.profileComplete = updateVals.profileComplete = true;

                if (req.user.userType === "recruiter") {
                    req.user.company = updateVals.company = req.body.company;
                    req.user.jobPosition = updateVals.jobPosition = req.body.jobtitle;
                }

                userInstance.update(updateVals);

                // req.logIn(req.user, function (err) {

                // if (!err) {
                req.session.save(function (err) {
                    res.redirect('/profile');
                });
                // } else {
                // res.redirect('/welcome');
                // }
                // });
            });
    });

    app.get('/profile', ourMiddleware.isLoggedIn, function (req, res) {
        var b = (req.query.username);
        if (b) {
            Models.User.findOne({
                    where: {
                        username: req.query.username
                    }
                })
                .then(function (user) {
                    if (user.username === req.user.username) {
                        res.redirect('/profile');
                    } else {
                        res.render('profileView', {
                            user: req.user,
                            profileUser: user,
                            title: user.nameFirst + " " + user.nameLast
                        });
                    }
                })
                .catch(function (err) {
                    console.log("This user doesn't exist (" + req.query.username + ").");
                    b = false;
                });
        }
        if (!b) {
            res.render('profile', {
                title: 'You',
                user: req.user,
                userNice: JSON.stringify(req.user, null, '\t')
            });
        }
    });

    app.get('/profile/edit', ourMiddleware.isLoggedIn, function (req, res) {
        res.render('profileEdit', {
            title: "Edit Profile",
            user: req.user
        });
    });

    app.post('/profile/edit', ourMiddleware.isLoggedIn, function (req, res) {
        Models.User.findOne({
                where: {
                    username: req.user.username
                }
            })
            .then(function (user) {
                var updateVals = {};
                req.user.addressFirstLine = updateVals.addressFirstLine = req.body.addressFirstLine;
                req.user.addressBuildingNo = updateVals.addressBuildingNo = req.body.addressBuildingNo ? req.body.addressBuildingNo : "";
                req.user.addressSecondLine = updateVals.addressSecondLine = req.body.addressSecondLine;
                req.user.addressPostalCode = updateVals.addressPostalCode = req.body.addressPostalcode;
                req.user.profileComplete = updateVals.profileComplete = true;
                req.user.nameFirst = updateVals.nameFirst = req.body.nameFirst;
                req.user.nameLast = updateVals.nameLast = req.body.nameLast;
                req.user.username = updateVals.username = req.body.username;

                user.update(updateVals);
                req.session.save(function (err) {
                    if (!err) {
                        res.redirect('/profile');
                    } else {
                        res.render('error', {
                            message: err
                        });
                    }
                });
            })
            .catch(function (err) {
                res.render('error', {
                    message: err
                });
            });
    });

    app.get('/profile/cv', ourMiddleware.isLoggedIn, function (req, res) {
        res.render('upload', {
            title: "Upload CV",
            picture: "",
            accept: "doc,pdf,docx,odt",
            multiple: "false",
            fieldName: "docCv",
            backUrl: "/profile"
        });
    });

    app.post('/profile/cv', ourMiddleware.isLoggedIn, function (req, res) {
        multer({
            storage: ourMiddleware.contentStore
        }).single('docCv')(req, res, function (err) {
            Models.User.findOne({
                    where: {
                        username: req.user.username
                    }
                })
                .then(function (userInstance) {
                    console.log("MADE IT TO HERE");
                    Models.File.create({
                            fileUri: path.join('content', 'doc', req.file.filename),
                            name: req.file.originalname,
                            username: req.user.username
                        })
                        .then(function (fileInstance) {
                            userInstance.update({
                                cvUri: fileInstance.id
                            });
                            res.redirect('/profile');
                        });
                })
                .catch(function (err) {
                    res.render('error', {
                        title: "error",
                        message: err
                    });
                });
        });
    });

    app.get('/profile/picture', ourMiddleware.isLoggedIn, function (req, res) {
        res.render('upload', {
            title: "Upload New Profile Picture",
            accept: "image/*",
            picture: req.user.profilePictureUri ? "/content/" + req.user.profilePictureUri : "",
            multiple: "false",
            fieldName: "imgProfile",
            backUrl: "/profile"
        });
    });

    app.post('/profile/picture', ourMiddleware.isLoggedIn, ourMiddleware.upload.single('imgProfile'), function (req, res) {
        Models.User.findOne({
                where: {
                    username: req.user.username
                }
            })
            .then(function (userInstance) {
                Models.File.create({
                        fileUri: path.join('content', 'img', req.file.filename),
                        name: req.file.originalname,
                        username: req.user.username
                    })
                    .then(function (fileInstance) {
                        userInstance.update({
                            profilePictureUri: fileInstance.id
                        });
                        res.redirect('/profile');
                    });
            })
            .catch(function (err) {
                res.render('error', {
                    title: "error",
                    message: err
                });
            });
    });

    app.get('/positions', ourMiddleware.isLoggedIn, function (req, res) {
        ourMiddleware.getPositions({
            closeDate: {
                $gte: new Date()
            }
        }).then(function (positions) {
            res.render('positions', {
                title: 'Jobs',
                user: req.user,
                positions: positions
            });
        });
    });

    app.get('/positions/search', ourMiddleware.isLoggedIn, function (req, res) {
        ourMiddleware.getPositions({
            nameText: {
                $iLike: "%" + req.query.q + "%"
            },
            closeDate: {
                $gte: new Date()
            }
        }).then(function (positions) {
            res.render('positions', {
                title: 'Jobs Searching for "' + req.query.q + '"',
                user: req.user,
                query: req.query.q,
                positions: positions
            });
        });
    });

    app.get('/positions/:id/view', ourMiddleware.isLoggedIn, function (req, res) {
        complexQueries.loadPosition(req.user, req.params.id)
            .then(function (data) {
                // error checking
                if (data.error === "") {
                    res.render('positionDetail', data);
                } else {
                    res.render('error', {
                        title: "Error",
                        message: data.error,
                        back_url: "/positions"
                    });
                }
            });
    });

    app.get('/positions/:id/apply', ourMiddleware.isLoggedIn, function (req, res) {
        Models.Position.findOne({
            where: {
                jobID: req.params.id
            }
        }).then(function (position) {
            if (position.closeDate > new Date()) {
                Models.User.findOne({
                        where: {
                            username: req.user.username
                        }
                    })
                    .then(function (userInstance) {
                        var hash = Models.Application.genId(userInstance.username);
                        Models.Application.create({
                                score: 0,
                                anonId: hash.slice(0, 5)
                            }).then(function (appInstance) {
                                userInstance.addApplication(appInstance)
                                    .then(function () {
                                        position.addApplication(appInstance)
                                            .then(function () {
                                                req.flash('successMessage', 'You successfully applied for "' + position.nameText + '"!');
                                                res.redirect('/');
                                            });
                                    });
                            })
                            .catch(function (err) {
                                res.render('error', {
                                    title: "Error",
                                    message: err,
                                    back_url: '/positions/apply/' + req.params.id
                                });
                            });
                    });
            } else {
                res.render('error', {
                    title: "Position Closed",
                    message: "This position is closed. Sorry!",
                    back_url: "/positions"
                });
            }
        });
    });

    app.get('/task/:id', ourMiddleware.isLoggedIn, function (req, res) {
        Models.Task.findOne({
                where: {
                    taskId: req.params.id
                }
            })
            .then(function (taskInstance) {
                // check that this user has an application for this.
                Models.Application.findOne({
                        where: {
                            positionId: taskInstance.positionId,
                            username: req.user.username
                        }
                    })
                    .then(function (applicationInstance) {
                        Models.Position.findOne({
                                where: {
                                    jobID: taskInstance.positionId
                                }
                            })
                            .then(function (positionInstance) {
                                if (new Date() < positionInstance.closeDate) {
                                    res.render('taskAttempt', {
                                        position: positionInstance,
                                        task: taskInstance,
                                        user: req.user,
                                        title: taskInstance.name
                                    });
                                } else {
                                    res.status(403).render('error', {
                                        title: "Cannot attempt task",
                                        message: "This position has closed, you cannot attempt it.",
                                        back_url: "/positions"
                                    });
                                }
                            });
                    })
                    .catch(function (err) {
                        res.render('error', {
                            title: "Oops",
                            message: "You can't complete this task as you have not applied for this job.",
                            back_url: "/positions/" + taskInstance.positionId + '/view'
                        });
                    });
            })
            .catch(function (err) {
                res.render('error', {
                    title: "Task not found",
                    message: "This task cannot be found.",
                    back_url: "/home"
                });
            });
    });

    app.post('/task/:id', ourMiddleware.isLoggedIn, function (req, res) {
        // Files are available via req.files.
        // Find the task.
        var fileNames = [];
        Models.Task.findOne({
                where: {
                    taskId: req.params.id
                }
            })
            .then(function (taskInstance) {
                // Find a related application.
                Models.Application.findOne({
                        where: {
                            username: req.user.username,
                            positionId: taskInstance.positionId
                        }
                    })
                    .then(function (applicationInstance) {
                        // what happens next depends on the task.
                        switch (taskInstance.taskType) {
                            case "txt":
                                ourMiddleware.upload.array()(req, res, function (err) {
                                    var content = req.body.txtContent;
                                    var name = Date.now() + '-' + md5(content) + '.md';
                                    var fpath = path.join('content', 'txt', name);
                                    fs.writeFileSync(path.join(__dirname, '../', fpath), content, 'utf8');
                                    Models.File.create({
                                            fileUri: fpath,
                                            name: name
                                        })
                                        .then(function (fileInstance) {
                                            Models.TaskAttempt.create({
                                                    filenames: [fileInstance.id],
                                                    taskId: taskInstance.taskId
                                                })
                                                .then(function (taskAttemptInstance) {
                                                    applicationInstance.addTaskAttempt(taskAttemptInstance)
                                                        .then(function () {
                                                            res.redirect('/positions/' + taskInstance.positionId + '/view');
                                                        });
                                                });
                                        })
                                        .catch(function (err) {
                                            res.render('error', {
                                                title: "Error",
                                                message: err
                                            });
                                        });
                                });
                                break;
                            case "img":
                                ourMiddleware.upload.array('imgFiles', taskInstance.maxImages ? taskInstance.maxImages : 8)(req, res, function (err) {
                                    if (err) {
                                        res.render('error', {
                                            title: "Error",
                                            message: "Something went wrong.<br />" + err,
                                            back_url: "task/" + req.params.id
                                        });
                                    }
                                    // Something did not go wrong.
                                    var fileJson = [];
                                    for (var f in req.files) {
                                        var data = {
                                            fileUri: path.join('content', 'img', req.files[f].filename),
                                            name: req.files[f].originalname,
                                            username: req.user.username
                                        };
                                        fileNames.push(data.fileUri);
                                        fileJson.push(data);
                                    }
                                    Models.File.bulkCreate(fileJson)
                                        .then(function () {
                                            return Models.File.findAll({
                                                where: {
                                                    fileUri: {
                                                        $in: fileNames
                                                    }
                                                }
                                            });
                                            // TODO: Filter this
                                        })
                                        .then(function (files) {
                                            filenames = [];
                                            for (f in files) {
                                                filenames.push(files[f].id);
                                            }
                                            Models.TaskAttempt.create({
                                                    filenames: filenames,
                                                    taskId: taskInstance.taskId
                                                })
                                                .then(function (taskAttempt) {
                                                    applicationInstance.addTaskAttempt(taskAttempt)
                                                        .then(function () {
                                                            res.redirect('/positions/' + taskInstance.positionId + '/view');
                                                        });
                                                });
                                        });
                                });
                                break;
                        }
                    })
                    .catch(function (err) {
                        res.status(403).render('error', {
                            title: "Error",
                            message: "You need to create an application before you can attempt this task.<br />Error details:<br />" + err,
                            back_url: "positions/" + taskInstance.positionId + '/view'
                        });
                    });
            })
            .catch(function (err) {
                res.status(404).render('error', {
                    title: "Not Found",
                    message: "That task cannot be found.",
                    back_url: "/positions"
                });
            });
    });

    app.get('/task/:id/view', ourMiddleware.isLoggedIn, function (req, res) {
        complexQueries.loadTaskAttempt(req.user, req.params.id)
            .then(function (data) {
                if (data.error === "") {
                    res.render('taskView', data);
                } else {
                    res.render('error', {
                        title: "Error",
                        message: "There was an error: <br />" + data.error,
                        back_url: "/positions"
                    });
                }
            })
            .catch(function (err) {
                res.render('error', {
                    message: "yo, " + err
                });
            });
    });

    app.get('/content/:id', ourMiddleware.isLoggedIn, function (req, res) {
        Models.File.findOne({
                where: {
                    id: req.params.id
                }
            })
            .then(function (file) {
                res.sendFile(path.join(__dirname, '../', file.fileUri));
            })
            .catch(function (err) {
                res.status(404).send('');
            });
    });

    app.get('/logout', function (req, res) {
        req.logout();
        req.session.destroy(function (err) {
            res.redirect('/');
        });
    });

    app.get('/delete', ourMiddleware.isLoggedIn, function (req, res) {
        res.render('deleteAccount', {
            title: "Delete Account",
            user: req.user,
            message: req.flash('message')
        });
    });

    app.post('/delete', ourMiddleware.isLoggedIn, function (req, res) {
        Models.User.findOne({
                where: {
                    username: req.user.username
                }
            })
            .then(function (user) {
                console.log(req.body.password);
                if (user.validPassword(req.body.password)) {
                    // delete this account
                    user.destroy()
                    .then(res.redirect('/logout'));
                } else {
                    req.flash('message', 'Error, incorrect password');
                    res.redirect('/delete');
                }
            })
            .catch(function (err) {
                res.status(500).render('error',{
                    message: err,
                    back_url: '/delete'
                });
            });
    });

};
