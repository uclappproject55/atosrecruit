/**
*  TASKHANDLING.JS
*  Obsolete handling of file uploading.
**/
var ourMiddleware = require('./middleware.js');

module.exports = {
    docHandler: function (req, res, task, application) {
    },
    txtHandler: function (req, res, task, application) {

    },
    imgHandler: function (req, res, task, application) {
        ourMiddleware.taskFiles.fields([
            {
                name: 'imgFiles',
                maxCount: task.maxImages
            }
        ], function (err) {
            if (err) {
                return res.status(500).render('error', {
                    title: "Error",
                    message: "Something went wrong:\n"+err
                });
            }
            console.log(length(req.files['imgFiles']) + " files saved.");
            return res.send(req.files['imgFiles'][0].path);
        });
    }
};
