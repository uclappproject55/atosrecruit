/**
*  LOGIN.JS
*  The routes used to log in and register.
**/
module.exports = function (app, passport) {
    app.post('/login', function (req, res, next) {
        passport.authenticate('local-login', {
            successRedirect: req.body.redirect? req.body.redirect : '/',
            failureRedirect: '/login' + (req.body.redirect? '?redirect='+encodeURIComponent(req.body.redirect) : ''),
            message: req.flash('loginMessage')
        })(req, res, next);
    });

    app.post('/register', function (req, res, next) {
        if (req.body.password !== req.body.passwordConfirm) {
            req.flash('signupMessage', 'The passwords do not match!');
            res.redirect('/register');
        } else {
            passport.authenticate('local-signup', {
                successRedirect: '/',
                failureRedirect: '/register',
                message: req.flash('signupMessage')
            })(req, res, next);
        }
    });
};
