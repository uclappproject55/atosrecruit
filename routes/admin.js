/**
*  ADMIN.JS
*  Defines a set of routes that only admins (recuiters) can access.
*  Some routes may override those defined in user.js.
**/
var ourMiddleware = require('./middleware.js');
var Models = require('../models/models.js');

module.exports = function (app, passport) {
    app.get('/home', function (req, res, next) {
        if (req.isAuthenticated() && req.user.profileComplete && req.user.userType === "recruiter") {
            ourMiddleware.getMyPositions(req.user.username, true).then(function (positions) {
                res.render('adminHome', {
                    title: "Admin Home",
                    user: req.user,
                    myPositions: positions
                });
            });
        } else return next();
    });

    app.get('/positions/create', ourMiddleware.isAdmin, function (req, res, next) {
        res.render('adminCreatePosition', {
            title: "Create New Position",
            user: req.user
        });
    });

    app.post('/positions/create', ourMiddleware.isAdmin, function (req, res) {
        console.log("DATE TYPE: " + typeof (req.body.startdate));
        var newPosition = {
            nameText: req.body.name,
            description: req.body.description,
            openDate: req.body.startdate,
            closeDate: req.body.enddate,
            createdBy: req.user.username
        };

        Models.User.findOne({
                where: {
                    username: req.user.username
                }
            })
            .then(function (userInstance) {
                Models.Position.create(newPosition)
                    .then(function (positionInstance) {
                        userInstance.addPosition(positionInstance)
                            .then(function () {
                                res.redirect('/');
                            });
                    })
                    .catch(function (err) {
                        req.flash('positionCreateError', err);
                    });
            });
    });

    app.get('/positions/:id/view', ourMiddleware.isLoggedIn, function (req, res, next) {
        if (req.user.userType == "recruiter") {
            Models.Position.findOne({
                    where: {
                        jobID: req.params.id
                    }
                })
                .then(function (pos) {
                    pos.getApplications()
                        .then(function (apps) {
                            pos.getTasks()
                                .then(function(tasks) {
                                    var usernames = [];
                                    for (var i in apps) {
                                        usernames.push(apps[i].username);
                                    }
                                    Models.User.findAll({
                                        where: {
                                            username: {
                                                $in: usernames
                                            }
                                        }
                                    })
                                    .then(function (users) {
                                        var applications = [];
                                        for (var i in apps) {
                                            var tmp = JSON.parse(JSON.stringify(apps[i]));
                                            tmp.user = users.filter(function (usr) {
                                                return usr.username === tmp.username;
                                            })[0];
                                            applications.push(tmp);
                                        }
                                        res.render('positionDetail', {
                                            title: pos.nameText,
                                            user: req.user,
                                            position: pos,
                                            tasks: tasks,
                                            applications: applications
                                        });
                                    });
                                });
                        });
                })
                .catch(function (err) {
                    res.render('error', {
                        title: 'Page not found',
                        user: req.user,
                        message: 'That was not found.',
                        back_url: '/home'
                    });
                });
        } else {
            next();
        }
    });

    app.get('/positions/:id/addTask', ourMiddleware.isAdmin, function (req, res) {
        Models.Position.findOne({
                where: {
                    jobID: req.params.id
                }
            })
            .then(function (position) {
                res.render('taskCreate', {
                    title: 'New Task',
                    user: req.user,
                    position: position
                });
            })
            .catch(function (err) {
                res.render('error', {
                    title: 'Error',
                    message: err,
                    back_url: '/positions'
                });
            });
    });

    app.post('/positions/:id/addTask', ourMiddleware.isAdmin, function (req, res) {
        var taskData = {
            maxPoints: req.body.maxpoints,
            name: req.body.name,
            description: req.body.description,
            taskType: req.body.type,
            maxImages: req.body.maximages ? req.body.maximages : '1',
            restrictedFileTypes: req.body.doctype ? req.body.doctype : '',
            textMaxLength: req.body.maxtxtlength ? req.body.maxtxtlength : '1024'
        };
        Models.Task.create(taskData)
            .then(function (taskInstance) {
                Models.Position.findOne({
                        where: {
                            jobID: req.params.id
                        }
                    })
                    .then(function (positionInstance) {
                        positionInstance.addTask(taskInstance)
                            .then(function () {
                                res.redirect('/positions/' + req.params.id + '/view');
                            });
                    });
            })
            .catch(function (err) {
                res.render('error', {
                    title: 'Error',
                    message: err,
                    back_url: '/positions/' + req.params.id + '/addTask'
                });
            });
    });
};
