/**
*  MIDDLEWARE.JS
*  A set of functions used for routing, some may be used as routing
*  middleware (e.g. checking that the user is logged in)
**/
var Models = require('../models/models.js');
var Promise = require('bluebird');
var multer = require('multer');
var md5 = require('js-md5');

var getPositions = function (constraints) {
    return new Promise(function (resolve) {

        Models.Position.findAll({
                where: constraints ? constraints : {},
                include: [Models.User]
            })
            .then(function (positions) {
                var data = [];
                positions.forEach(function (pos) {
                    data.push(pos.get());
                });
                resolve(data); // we are done.
            })
            .catch(function (err) {
                console.log(err);
                resolve([]); // you FAIL
            });
    });
};

var getMyPositions = function (username, hideOldPositions) {
    return new Promise(function (resolve) {
        Models.User.findOne({
                where: {
                    username: username
                }
            })
            .then(function (userInstance) {
                userInstance.getPositions(hideOldPositions ? {
                        where: {
                            closeDate: {
                                $gte: new Date()
                            }
                        }
                    } : {})
                    .then(function (positions) {
                        var data = [];
                        positions.forEach(function (pos) {
                            data.push(pos.get());
                        });
                        resolve(data);
                    });
            })
            .catch(function (err) {
                resolve([]);
            });
    });
};

var isLoggedIn = function (req, res, next) {
    if (req.user) {
        if (req.user.profileComplete || req.path === "/welcome") {
            return next();
        } else {
            return res.redirect('/welcome');
        }
    } else {
        req.flash('loginMessage', 'You need to be signed in to see this.');
        req.user = null;
        return res.redirect('/login?redirect=' + encodeURIComponent(req.path));
    }
};

var isAdmin = function (req, res, next) {
    if (req.isAuthenticated()) {
        if (req.user.profileComplete || req.path === "/welcome") {
            if (req.user.userType === "recruiter") {
                return next();
            } else {
                res.status(403);
            }
        } else {
            res.redirect('/welcome');
        }
    } else {
        req.flash('loginMessage', 'You need to be signed in to see this.');
        res.redirect('/login?redirect=' + encodeURIComponent(req.path));
    }
};

var getUserApplications = function (username, includeExpired) {
    return new Promise(function (resolve) {
        Models.Application.findAll({
                where: {
                    username: username
                },
                include: [Models.Position]
            })
            .then(function (applications) {
                var data = [];
                applications.forEach(function (application) {
                    var appdata = application.get();
                    if (!includeExpired || appdata.position.closeDate > new Date())
                        data.push(appdata);
                });
                resolve(data);
            })
            .catch(function (err) {
                resolve([]);
            });
    });
};

// Multer configs.
var contentStore = multer.diskStorage({
    destination: function (req, file, cb) {
        switch (true) {
            case /^txt.*/.test(file.fieldname):
                cb(null, './content/txt');
                break;
            case /^doc.*/.test(file.fieldname):
                cb(null, './content/doc');
                break;
            case /^img.*/.test(file.fieldname):
                cb(null, './content/img');
                break;
        }
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + "-" + md5(file.originalname) + file.originalname.match(/\.(\w|\d)+$/)[0]);
    }
});

var upload = multer({
    storage: contentStore
});

module.exports = {
    getPositions: getPositions,
    getMyPositions: getMyPositions,
    isLoggedIn: isLoggedIn,
    isAdmin: isAdmin,
    getUserApplications: getUserApplications,
    contentStore: contentStore,
    upload: upload
};
