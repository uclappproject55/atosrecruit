/**
*  COMPLEXQUERIES.JS
*  Defines a set of more complex queries that may be used in routes.
*  Cleans up the code used for routes.
**/
var Models = require('../models/models.js');
var Promsise = require('bluebird');
var fs = require('fs');
var path = require('path');
var marked = require('marked');

var exports = {};

exports.isEmptyObj = function (obj) {
    for (var prop in obj) {
        return false;
    }
    return true;
};

exports.areTasksCompleted = function (tasks, taskAttempts) {
    var returnData = {
        tasks: [],
        taskAttempts: []
    };

    for (var task in tasks) {
        // does this task exist in taskAttempts?
        var tempAttempt = {};
        for (var taskAttempt in taskAttempts) {
            if (taskAttempts[taskAttempt].taskId === tasks[task].taskId) {
                tempAttempt = taskAttempts[taskAttempt];
            }
        }
        // if exists, push it onto taskAttempts. Else, just tasks.
        if (!exports.isEmptyObj(tempAttempt)) {
            var taskVal = tasks[task];
            taskVal.isCompleted = tempAttempt.isCompleted;
            taskVal.score = tempAttempt.score;
            taskVal.submissionDate = tempAttempt.createdAt;
            taskVal.filenames = tempAttempt.filenames;
            returnData.taskAttempts.push(taskVal);
        } else {
            returnData.tasks.push(tasks[task]);
        }
    }

    return returnData;
};

exports.loadTaskAttempt = function (user, taskId) {
    return new Promise(function (resolve) {
        // first, find the related task.
        Models.Task.findOne({
                where: {
                    taskId: taskId
                }
            })
            .then(function (task) {
                // find the related application.
                Models.Application.findOne({
                    where: {
                        positionId: task.positionId,
                        username: user.username
                    }
                })
                .then(function (applicationInstance) {
                    // find the task attempt.
                    applicationInstance.getTaskAttempts({
                        where: {
                            taskId: task.taskId
                        }
                    })
                    .then(function (taskAttempts) {
                        // If txt task, then let's open and send that markdown file.
                        var data = {};
                        if (task.taskType === "txt") {
                            Models.File.findOne({
                                where: {
                                    id: taskAttempts[0].filenames[0]
                                }
                            })
                            .then(function (file) {
                                var md = fs.readFileSync(file.fileUri,'utf8');
                                data.content = marked(md);
                                data.task = task;
                                data.title = task.name;
                                data.user = user;
                                data.error = "";
                                console.log(data.content);
                                return resolve(data);
                            })
                            .catch(function (err) {
                                resolve({
                                    error: "Error:\n"+err
                                });
                            });
                        }
                        else {
                            data.content = taskAttempts[0].filenames;
                            data.task = task;
                            data.user = user;
                            data.title = task.name;
                            data.error = "";
                            resolve(data);
                        }
                    })
                    .catch(function (err) {
                        resolve({error: err});
                    });
                })
                .catch(function (err) {
                    resolve({
                        error: "No application found.\n" + err
                    });
                });
            })
            .catch(function (err) {
                resolve({
                    error: "Task not found."
                });
            });
    });
};

exports.loadPosition = function (user, positionId) {
    return new Promise(function (resolve) {
        // Search for positions
        Models.Position.findOne({
                where: {
                    jobID: positionId
                }
            })
            .then(function (posInstance) {
                // If position is still open.
                if (posInstance.closeDate > Date.now()) {
                    // get tasks
                    posInstance.getTasks()
                        .then(function (tasks) {
                            // get the application
                            Models.Application.findOne({
                                    where: {
                                        positionId: positionId,
                                        username: user.username
                                    }
                                })
                                .then(function (applicationInstance) {
                                    // get the tasks attempted
                                    applicationInstance.getTaskAttempts()
                                        .then(function (taskAttempts) {
                                            // Split tasks into those completed and those not.
                                            var tasksSorted = exports.areTasksCompleted(tasks, taskAttempts);
                                            resolve({
                                                title: posInstance.nameText,
                                                user: user,
                                                tasksPending: tasksSorted.tasks,
                                                tasksCompleted: tasksSorted.taskAttempts,
                                                position: posInstance,
                                                application: applicationInstance,
                                                error: ""
                                            });
                                        })
                                        .catch(function (err) {
                                            resolve({
                                                error: err
                                            });
                                        });
                                })
                                .catch(function (err) {
                                    resolve({
                                        title: posInstance.nameText,
                                        user: user,
                                        position: posInstance,
                                        error: ""
                                    });
                                });
                        })
                        .catch(function (err) {
                            resolve({
                                error: "There was an error: <br />" + err
                            });
                        });
                } else {
                    resolve({
                        error: "This position is closed. Sorry!"
                    });
                }
            })
            .catch(function (err) {
                // FAIL
                resolve({
                    error: "Something happened. <br />" + err
                });
            })
    });
};

module.exports = exports;
