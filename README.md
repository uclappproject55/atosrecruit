# Atos Recruitment Portal

A web portal for the Atos graduate recruitment program.

## Getting the project up and running
The project is using the **stable** release of node.js (4.x). We recommend using [Node Version Manager](https://github.com/creationix/nvm) (NVM) to manage your installs and keep them up to date.

The current version of Node.js is set with the `.nvmrc` file. Take a look at `post-receive-sample.sh` for an example on how to use this to keep your Node.js version and packages up to date.

Use `npm` to install node modules (for copyright, size, and updating reasons, they are excluded from the repository):

`$ npm install`

Also, Bower is used to manage the client-side dependencies (e.g., jQuery, Bootstrap etc.). You'll need to install bower as an administrator and then run it.

    $ npm install -g bower
    $ bower install

All these commands should be executed in the atosRecruitPortal directory.

Finally, you will also need to have PostgreSQL database installed. Available from most good package managers, or from their [project webpage](http://postgresql.org) if not.

You will also need to add your database details to a file, config.js, inside the config directory. You can find an example in the same directory.

To run the recruit server, you want to execute

    $ node index.js

Or, you can run your favourite CLI tool for running Node processes (e.g., Forever, nodemon, upstart...).

## Directory hierachy
    # dir = atosRecruitPortal
    ├-- models/
    ├-- routes/
    ├-- views/
    |   ├-- partials/
    |   └-- layouts/
    ├-- static/
    |   ├-- img/
    |   ├-- css/
    |   └-- js/
    ├-- content/
    |   ├-- img/
    |   └-- files/
    ├-- bower.json
    ├-- package.json
    ├-- gulpfile.js
    └-- index.js

`models` - where models for the database are stored.

`routes` - JS files defining the URL routes of the server.

`views` - contain Handlebars templates for the pages.

`views/partials` - reusable template elements.

`views/layouts` - Page layouts.

`content` - content shared by users.

`bower.json` - Bower config file which declares dependencies the project uses.

`package.json` - NPM config file which declares server dependencies.

`gulpfile.js` - Used to define tasks that only need to be ran once (e.g., generating CSS files).

`index.js` - Server init file.
