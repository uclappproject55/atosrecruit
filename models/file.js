/**
*  FILE.JS
*  A model for files saved to the filesystem.
*  Could be extended in the future for user attribution, comments?
*/
var Sequelize = require('sequelize');
var md5 = require('js-md5');

module.exports = {
    attributes: {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        fileUri: Sequelize.STRING,
        username: Sequelize.STRING,
        name: Sequelize.STRING
    },
    options: {
        freezeTableName: true,
        classMethods: {
            // files are stored as a md5 hash.
            generateFilename : function (filename) {
                md5(filename);
                return md5.create();
            }
        }
    }
};
