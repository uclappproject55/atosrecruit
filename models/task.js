/**
*  TASK.JS
*  Represents a task for a job position that a user can complete.
**/
var Sequelize = require('sequelize');

module.exports = {
    attributes: {
        taskId : {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        maxPoints: Sequelize.INTEGER,
        name: Sequelize.STRING,
        description: Sequelize.TEXT,
        taskType: Sequelize.STRING(3), //img, doc, txt
        maxImages: Sequelize.INTEGER,
        restrictedFileTypes: Sequelize.STRING, // comma separated
        textMaxLength: Sequelize.INTEGER
    },

    options: {
        freezeTableName: true
    }
};
