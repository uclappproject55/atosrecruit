/**
*  APPLICATION.JS
*  An application for a job position.
**/
var Sequelize = require('sequelize');
var md5 = require('js-md5');

module.exports = {
    attributes: {
      score: Sequelize.INTEGER,
      anonId: Sequelize.STRING(5)
    },
    options: {
        freezeTableName: true,
        classMethods: {
            genId: function (username) {
                return md5(username);
            }
        }
    }
};
