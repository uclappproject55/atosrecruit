/**
*  TASKATTEMPT.JS
*  A user's attempt at a task.
**/
var Sequelize = require('sequelize');

module.exports = {
    attributes: {
        score: Sequelize.INTEGER,
        isCompleted: Sequelize.BOOLEAN,
        filenames: Sequelize.ARRAY(Sequelize.STRING)
    },
    options: {
        freezeTableName: true
    }
};
