/**
*  POSITION.JS
*  Represents a job position that a user can apply for.
**/
var Sequelize = require('sequelize');

module.exports = {
    attributes: {
        jobID: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nameText: Sequelize.STRING,
        description: Sequelize.STRING,
        salary: Sequelize.FLOAT,
        openDate: Sequelize.DATE,
        closeDate: Sequelize.DATE,
        successfulCandidate: Sequelize.STRING
    },
    options: {
        freezeTableName: true
    }
};
