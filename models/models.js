/**
*  MODEL.JS
*  Imports the Object models and defines the relations between them.
*/
var UserModel = require('./user.js');
var PositionModel = require('./position.js');
var ApplicationModel = require('./application.js');
var TaskModel = require('./task.js');
var TaskAttemptModel = require('./taskAttempt.js');
var FileModel = require('./file.js');
var connection = require('../config/sequelize.js');

var m = {
  User: connection.define('user', UserModel.attributes, UserModel.options),
  Position: connection.define('position', PositionModel.attributes, PositionModel.options),
  Application: connection.define('application', ApplicationModel.attributes, ApplicationModel.options),
  Task: connection.define('task', TaskModel.attributes, TaskModel.options),
  TaskAttempt: connection.define('taskAttempt', TaskAttemptModel.attributes, TaskAttemptModel.options),
  File: connection.define('file', FileModel.attributes, FileModel.options)
};

// Positions relations.
m.User.hasMany(m.Position, {
  foreignKey: 'createdBy',
  onDelete: 'cascade'
});
m.Position.belongsTo(m.User, {
  foreignKey: 'createdBy'
});

// Application relations.
m.User.hasMany(m.Application, {
  foreignKey: 'username',
  onDelete: 'cascade'
});
m.Application.belongsTo(m.User, {
  foreignKey: 'username'
});
m.Position.hasMany(m.Application, {
  foreignKey: 'positionId',
  onDelete: 'cascade'
});
m.Application.belongsTo(m.Position, {
  foreignKey: 'positionId'
});

// task relations.
m.Position.hasMany(m.Task, {
    foreignKey: 'positionId',
    onDelete: 'cascade'
});
m.Task.belongsTo(m.Position, {
    foreignKey: 'positionId'
});

// task attempt relations.
m.Task.hasMany(m.TaskAttempt, {
    foreignKey: 'taskId',
    onDelete: 'cascade'
});
m.TaskAttempt.belongsTo(m.Task, {
    foreignKey: 'taskId'
});
m.Application.hasMany(m.TaskAttempt, {
    foreignKey: 'applicationId',
    onDelete: 'cascade'
});
m.TaskAttempt.belongsTo(m.Application, {
    foreignKey: 'applicationId'
});

connection.sync();

module.exports = m;
