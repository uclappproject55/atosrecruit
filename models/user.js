/**
*  USER.JS
*  Defines a user, comes with methods for generating and checking password
*  hashes.
**/
var Sequelize = require('sequelize');
var bcrypt = require('bcrypt-nodejs');

module.exports = {
    attributes: {
        username: {
            type: Sequelize.STRING,
            primaryKey: true,
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
        password: Sequelize.STRING,
        addressBuildingNo: Sequelize.STRING,
        addressFirstLine: Sequelize.STRING,
        addressSecondLine: Sequelize.STRING,
        addressTown: Sequelize.STRING,
        addressPostalCode: Sequelize.STRING,
        addressCountryCode: Sequelize.STRING,
        nameFirst: Sequelize.STRING,
        nameMiddle: Sequelize.STRING,
        nameLast: Sequelize.STRING,
        userType: Sequelize.STRING(10),
        profilePictureUri: Sequelize.STRING, // references a File.
        cvUri: Sequelize.INTEGER, // references a File.
        company: Sequelize.STRING,
        jobPosition: Sequelize.STRING,
        profileComplete: Sequelize.BOOLEAN
    },
    options: {
        freezeTableName: true,
        classMethods: {
            generateHash: function (password) {
                return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
            }
        },
        instanceMethods: {
            validPassword: function (password) {
                return bcrypt.compareSync(password, this.password);
            }
        }
    }
};
