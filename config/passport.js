/**
* PASSPORT.JS
* ===========
* This file contains the methods used to sign up and authenticate users,
* Using passport's local strategy and is saved to the database.
**/
var LocalStrategy = require('passport-local').Strategy;
var Model = require('../models/models.js');
var bcrypt = require('bcrypt-nodejs');

module.exports = function (passport) {


    passport.serializeUser(function (user, done) {
        done(null, user.username);
    });

    passport.deserializeUser(function (username, done) {
        Model.User.findOne({
            where: {
                username: username
            }
        })
        .then(function (userInstance) {
            done(null, userInstance);
        })
        .catch(function (err) {
            done(err, null);
        });
    });

    passport.use('local-login', new LocalStrategy({
                // by default, local strategy uses username and password, we will override with email
                usernameField: 'email',
                passwordField: 'password',
                passReqToCallback: true // allows us to pass back the entire request to the callback
            },
            function (req, username, password, done) {
                Model.User.findOne({
                    where: {
                        username: username
                    }
                }).then(function (user) {
                    if (user === null) {
                        return done(null, false, req.flash("loginMessage", "Incorrect details."));
                    }

                    if (user.validPassword(password)) {
                        return done(null, user);
                    } else {
                        return done(null, false, req.flash("loginMessage", "Incorrect details."));
                    }
                });
            }
        )),

        passport.use('local-signup', new LocalStrategy({
                usernameField: 'email',
                passwordField: 'password',
                passReqToCallback: true
            },
            function (req, username, password, done) {
                console.log("Creating user " + req);
                if (!username || !password || !req.body.passwordConfirm) {
                    return done(null, false, req.flash("signupMessage", "Please fill in the fields correctly."));
                }

                if (password !== req.body.passwordConfirm) {
                    return done(null, false, req.flash("signupMessage", "The passwords do not match."));
                }
                var newUser = Model.User.build({
                    username: username,
                    password: Model.User.generateHash(password),
                    nameFirst: req.body.firstname,
                    nameLast: req.body.lastname,
                    userType: req.body.type ? req.body.type : "applicant"
                });

                newUser.save()
                    .then(function () {
                        done(null, newUser);
                    })
                    .catch(function (err) {
                        done(null, false, req.flash("signupMessage", JSON.stringify(err)));
                    });
            }));

};
