// Sets up the database connection, using a user-defined conf.js containing the login details.
var Sequelize = require('sequelize');

var conf = require('./conf.js');

module.exports = new Sequelize('postgres://' + conf.db.user + ':' + conf.db.pass + '@127.0.0.1:5432/atos-data');
