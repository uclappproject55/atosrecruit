/**
*  INDEX.JS
*  The main file for the server. This should be run using node, nodemon,
*  forever, or your favourite Node.js program.
**/

// MEAN standard modules.
var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var fs = require('fs');

// variables.
var port = process.env.PORT || 8080; // Will be port 8080 unless told otherwise.
var sequelize = require('./config/sequelize.js');

// Additional imported modules.
var exphbs = require('express-handlebars');
var flash = require('connect-flash');
var morgan = require('morgan');
var SequelizeStore = require('connect-sequelize')(session);

// auth modules.
var passport = require('passport');

// Create the content directories if they do not already exist.
try {
    fs.mkdirSync("content");
}
catch (e) {
    console.log("content already exists, sweet.");
}
['txt','doc','img'].forEach(function (el) {
    try {
        fs.mkdirSync(path.join("content",el));
    }
    catch (e) {
        console.log("content/"+el+" already exists, sweet.");
    }
});

// log stuff.
app.use(morgan('dev'));

// handlebars setup
var hbs = exphbs.create({
    helpers: require('./views/helpers.js'),
    defaultLayout: 'default',
    extname: '.hbs'
});
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', hbs.engine);
app.set('view engine', 'hbs');

// Enable request body parsing and other stuff.
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

// Parse cookies.
app.use(cookieParser());

// Create tables if they don't already exist.
sequelize.sync();

// Set up session management (mmm, cookies.)
app.use(session({
    secret: 'nodeisgreat',
    proxy: true,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 900000
    }
}));

// Use flash (A way of passing messages back to the page.)
app.use(flash());

// Init user services.
require('./config/passport.js')(passport);
app.use(passport.initialize());
console.log("creating passport stuffs");
app.use(passport.session());

// Routing, precedence gived to admin.js routes.
require('./routes/login.js')(app, passport);
require('./routes/admin.js')(app, passport);
require('./routes/user.js')(app, passport);
require('./routes/routes.js')(app, passport);

// static files (assets and things installed via bower.)
app.use('/static', express.static(path.join(__dirname, '/static')));
app.use('/ext', express.static(path.join(__dirname, '/bower_components')));

// If no route can handle the request, we throw a 404.
app.use(function (req, res, next) {
    res.status(404).render('404', {
        title: "Not found"
    });
});

// Let's bring this server on.
var server = app.listen(port, function () {
    console.log('Server begin');
});
