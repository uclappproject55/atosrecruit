/**
*  GULPFILE.JS
*  A set of tasks that can be executed with the Gulp task runner.
**/
var gulp = require('gulp');
var sass = require('gulp-ruby-sass');

// Compile Sass -> CSS
gulp.task('sass', function() {
    return sass('static/css/main.scss')
        .on('error', sass.logError)
        .pipe(gulp.dest('static/css'));
})

// Watch for changes to Sass files.
gulp.task('watch', function() {
    gulp.watch('static/css/*.scss', ['sass']);
})

// By default, gulp will compile CSS.
gulp.task('default', ['sass']);
