#!/bin/bash

# An example Git hook for setting up the app on your own machine, using
# NVM for Node.js versioning, as well as the npm command line tools Bower, Gulp
# and Forever.

DEPLOY_DIR="/var/www/app"

echo "Deploying"
GIT_WORK_TREE="${DEPLOY_DIR}" git checkout -f master

echo "Loading nvm, node, npm"
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

echo "Running \`npm install\`"
cd $DEPLOY_DIR
CURRENT_VERSION=$(nvm version)
nvm install $(cat .nvmrc)
npm install
nvm reinstall-packages $CURRENT_VERSION
bower install
gulp sass

forever restart $DEPLOY_DIR/index.js
