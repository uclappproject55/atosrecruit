/**
*  HELPERS.JS
*  A set of helpers that can be called in any handlebars template.
**/
var dateFormats = {
    shortd: "DD/MM/YYYY",
    longd: "Do of MMMM YYYY (dddd)"
};

module.exports = {
    IfEq: function (v1, v2, options) {
        if (v1 && v2 && (v1.toLowerCase() === v2.toLowerCase())) {
            return options.fn(this);
        }
        return options.inverse(this);
    },
    ToUpper: function (options) {
        return options.fn(this).toUpperCase();
    },
    FormatDate: function (date, format) {
        return '<time datetime="'+date+'" data-format="'+dateFormats[format]+'"></time>';
    }
};
