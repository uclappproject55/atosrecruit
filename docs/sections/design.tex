\section{Backend Design}
\subsection{Database}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{relations}
    \caption{The data relations for the system, in UML format.}
    \label{fig:relations}
\end{figure}

\begin{table}[]
    \centering
    \begin{subtable}{\textwidth}
        \centering
        \begin{tabular}{|l|l|l|l|}
            \hline
            Data Item  & Name      & Type   & Source     \\ \hline
            Email      & \texttt{email}     & String & User input \\
            First Name & \texttt{firstName} & String & User input \\
            Last Name  & \texttt{lastName}  & String & User input \\
            Password   & \texttt{password}  & String & Hashed user input \\
            Recruiter Status & \texttt{isRecruiter} & Boolean & Admin input \\
            Address & \texttt{address} & list of String  & User input \\
            \hline
        \end{tabular}
        \caption{User data}
    \end{subtable}
    \begin{subtable}{\textwidth}
        \centering
        \begin{tabular}{|l|l|l|l|}
            \hline
            Position ID & \texttt{positionId} & Integer & Auto generated \\
            Position Name & \texttt{name} & String & Recruiter input \\
            Position Description & \texttt{description} & String & Recruiter input \\
            Salary & \texttt{salary} & Integer & Recruiter input\\
            Start Date & \texttt{startDate} & DateTime & Auto generated\\
            End Date & \texttt{closeDate} & DateTime & Recruiter input\\
            Recruiter & \texttt{createdBy} & String & Automatically set\\
            \hline
        \end{tabular}
        \caption{Position data}
    \end{subtable}
    \begin{subtable}{\textwidth}
        \centering
        \begin{tabular}{|l|l|l|l|}
            \hline
            Task ID & \texttt{taskId} & Integer & Auto generated \\
            Task Name & \texttt{name} & String & Recruiter input \\
            Task Description & \texttt{description} & String & Recruiter input \\
            Points Available & \texttt{points} & Integer & Recruiter input\\
            Position & \texttt{positionId} & Integer & Set by system\\
            \hline
        \end{tabular}
        \caption{Task data}
    \end{subtable}
    \label{tab:dataDict}
    \caption{Initial data dictionary}
\end{table}

To begin planning the database, I thought of all of the possible data objects that would be used in the system and listed them down with their attributes; this was written into the data dictionary (table~\ref{tab:dataDict}). Then, I normalised them to prevent data being repeated as much as possible. Next, I connected these objects together and then finally defined the multiplicity, or how many of one object is related to another. See figure~\ref{fig:relations} for the final design in UML format.


\section{Interface Design}
Two options were explored with the overarching design of the system, in terms of "gamifying" the recruitment process. One option suggested by the client was similar to what one can find in the popular social media game \textit{Farmville}~\cite{farmville}, where the applicant would have to plant seeds that represent job applications and nuture them into plants (getting the job) by completing tasks (see figure~\ref{fig:farmville})

The other option was to use a progression system similar to what can be found on social media sites like \textit{LinkedIn}~\cite{linkedin}, where the user is invited to complete tasks to complete a progress bar. An example is given in figure~\ref{fig:linkedin}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\textwidth]{farmville}
    \caption{A screenshot from the social network game, Farmville.~\cite{farmvilleImg}}
    \label{fig:farmville}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\textwidth]{linkedin}
    \caption{A screenshot from the social media site, LinkedIn.}
    \label{fig:linkedin}
\end{figure}

It was decided that the "cartoon garden" approach may seem a little condescending towards the user, so I opted for the progression-style system, where the applicant would have to score points as their application progressed. Figure~\ref{fig:paperDesign} shows the paper prototypes of the applicant and recruiter workflows, detailing less on interface specifics, but focusing on the actions that each user would take. Figure~\ref{fig:designdigital} shows the first digital design\textemdash for this, I used the Material Design Lite library~\cite{www-mdl} to quickly iterate the layout.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{design1-paper}
    \includegraphics[width=0.8\textwidth]{design2-paper}
    \caption{Paper prototypes, showing the recruiters' workflow (first) and the applicants' workflow (second).}
    \label{fig:paperDesign}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{digital-mockup}
    \caption{The first digital design of the interface.}
    \label{fig:designdigital}
\end{figure}


\subsection{Final Design}
\subsubsection{Fonts}
\begin{figure}[h]
    \begin{subfigure}{\textwidth}
        \centering
        {\fontsize{2cm}{3cm}\color{colorText}\fontspec{Bree Serif}Atos Recruit}
        \caption{Heading font, Bree Serif.}
        \label{fig:fontTitle}
    \end{subfigure}

    \begin{subfigure}{\textwidth}
        \centering
        {\fontsize{0.4cm}{0.6cm}\color{colorText}\fontspec{Noto Sans}We've created your profile below. Please take the time to check through it and make sure it's all correct.}
        \caption{Body font, Noto Sans.}
        \label{fig:fontBody}
    \end{subfigure}
\end{figure}
For the heading fonts, I chose a font that was as similar as possible to what Atos use themselves for their own branding. I couldn't use their actual font because it was non-free, so I found Bree Serif, a free font found in the Google Fonts library. It suits the purpose well; the typeface is bold, professional and is more interesting than a generic sans typeface that is currently used by many sites worldwide. Figure~\ref{fig:fontTitle} shows an example of the typeface in use.

For the body text, I wanted to use a font that was easy to read on all form factors, as well as being clear, concise, modern, and would support a wide variety of languages in case localisation is used in the future. So naturally, I chose Noto Sans, a font which aims to support as many \gls{unicode} characters as possible. An example of this font is shown in figure~\ref{fig:fontBody}. The contrast between the serif heading font and the sans-serif body font makes on-screen text very easy to read, as well as interesting too.

\subsubsection{Colour}
\begin{figure}[h]
    \centering
    \begin{subfigure}[t]{0.32\textwidth}
        \centering
        \begin{tikzpicture}
            \filldraw[very thick, fill=colorBlue, draw=colorDoc] (1,1) circle (1.5cm);
        \end{tikzpicture}
        \caption{\#0066a1}
    \end{subfigure}
    \begin{subfigure}[t]{0.32\textwidth}
        \centering
        \begin{tikzpicture}
            \filldraw[very thick, fill=colorGrey, draw=colorDoc] (1,1) circle (1.5cm);
        \end{tikzpicture}
        \caption{\#404040.}
    \end{subfigure}
    \begin{subfigure}[t]{0.32\textwidth}
        \centering
        \begin{tikzpicture}
            \filldraw[very thick, fill=colorText, draw=colorDoc] (1,1) circle (1.5cm);
        \end{tikzpicture}
        \caption{\#222222.}
    \end{subfigure}
    \caption{Brand colours used, from left to right: primary colour, secondary colour, and the text body colour.}
    \label{fig:colours}
\end{figure}

The colour choices (seen in figure~\ref{fig:colours}) were very easy to choose. The primary colour is taken from Atos' logo, and the secondary colour is taken from Atos' website. Finally, the text body colour is just a standard black. The background of the page design is white\textemdash keeping the majority of the colours black on white with a little accent means that the content is not distracting to the user. Elements on the page are easy to read, and the colours have a contrast ratio of about 16; this means that the page easily conforms to the Web Content Accesibility Guidelines~\cite{world2008web}. The primary colour on white passes the AA standard whilst the text on the white background passes the AAA standard.

\subsection{UI Elements}
Part of a consistent design is having well-thought out, consistent interface elements. The \gls{ui} for my project would aim to use few colours, solid outlines and use subtle animations to show what can be clicked and what cannot.
\subsubsection{Navigation Bar}
\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{nav1}
        \caption{Wide screen}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{nav2}
        \caption{Mobile screen}
    \end{subfigure}
    \caption{The navigation bar for different screen sizes.}
    \label{fig:uinav}
\end{figure}
The navigation bar is a great example of how responsive design was applied to the layout of the application. Figure~\ref{fig:uinav} shows the two designs that are used in the final system. The first state is what is seen when the page is viewed from a PC or laptop, whilst the second is what is seen when the page is viewed on a mobile device. This means that the menu is folded away when screen space is limited, allowing more space for the content. On a desktop screen/widescreen device, there is more breathing room for on-screen elements to space themselves out.

\subsubsection{Buttons}
\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{button1}
        \caption{Initial state}
    \end{subfigure}
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{button2}
        \caption{Hover state}
    \end{subfigure}
    \caption{A button element.}
    \label{fig:uibutton}
\end{figure}
Figure~\ref{fig:uibutton} shows the two main states of the button element. When the pointer hovers over the button, it will rise up a little to show that it can be clicked on. When clicked, it will recede into the page, giving the impression of an actual button being clicked.

\subsubsection{Text Boxes}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{textbox1}
    \caption{A text box element.}
    \label{fig:uiText}
\end{figure}
Figure~\ref{fig:uiText} shows an example of a text box used on the web pages. I recognised the issue that text with a single line underneath did not especially look like something that the user could click on and input text; so I added a subtle animation when the pointer hovers over the text box, where the line would darken and shift downwards a little bit.

\subsubsection{Screen Reader Support}
I have used a collection of different features on the web pages to ensure that screen readers can effectively use the web page. The simplest thing I did was to use \texttt{alt} attributes for images, so screen readers can describe what the image is (for instance, the Atos logo would be read as "Atos").

Another feature I added was on the navigation bar. When a sighted person looks at the bar, they can tell what page they are on because it is highlighted in a slightly different shade of grey. When a screen reader sees it, it will read the link text as well as a hidden element which says that it is the "current" page.

Finally, I have made liberal use of \acrshort{html}5's \glspl{semanticElement}. This means that screen readers not only know the content of an element, but also the purpose of that element. From using the \texttt{<nav>} tag, the reader knows that the content of it is the site navigation.
